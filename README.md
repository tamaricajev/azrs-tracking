## AZRS-TRACKING

##### Projekat u okviru kursa Alati za razvoj softvera na Matematičkom fakultetu. Služi za praćenje zadataka u okviru kojih se primenjuju alati. Zapravo u ovom projektu se vodi evidencija o progresu zadatka i opisuju se načini upotrebe alata koji su u tom zadatku korišćeni. Alati se primenjuju na projekat [_**STRATEGO**_](https://gitlab.com/tamaricajev/10-stratego) koji je razvijan 2020/2021. godine u okviru kursa Razvoj softvera na Matematičkom fakultetu. Grupni projekat [_**STRATEGO**_](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/10-stratego) možete pogledati [ovde](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/10-stratego) .

### Alati koji su primenjeni:
- CMake
- Clang Tidy
- Clang Format
- Clang Analyzer
- Valgrind
- Git Hook 
- GCov
- GammaRay
- Doxygen
- Docker
- Git
- GDB 

Nacin primene alata i spisak primenjenih alata nalazi se [ovde](https://gitlab.com/tamaricajev/azrs-tracking/-/issues?sort=created_date&state=all).




